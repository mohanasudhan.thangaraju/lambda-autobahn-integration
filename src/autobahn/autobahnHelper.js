'use strict';

/*
 * PEARSON PROPRIETARY AND CONFIDENTIAL INFORMATION SUBJECT TO NDA
 * Copyright © 2018 Pearson Education, Inc.
 * All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Pearson Education, Inc.  The intellectual and technical concepts contained
 * herein are proprietary to Pearson Education, Inc. and may be covered by U.S. and Foreign Patents,
 * patent applications, and are protected by trade secret or copyright law.
 * Dissemination of this information, reproduction of this material, and copying or distribution of this software
 * is strictly forbidden unless prior written permission is obtained
 * from Pearson Education, Inc.
 */

/**
 * Processes the Autobahn messages received
 * @param {*} data
 */
const processReceivedData = async (data) => {
    if (data && data[0].Body) {
        const payload = JSON.parse(data[0].Body).payload;
        const autobahnMetadata = JSON.parse(data[0].Body).autobahnMetadata;
        console.log({payload, autobahnMetadata});
    }
};

module.exports = { processReceivedData };
