'use strict';

/*
 * PEARSON PROPRIETARY AND CONFIDENTIAL INFORMATION SUBJECT TO NDA
 * Copyright © 2018 Pearson Education, Inc.
 * All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Pearson Education, Inc.  The intellectual and technical concepts contained
 * herein are proprietary to Pearson Education, Inc. and may be covered by U.S. and Foreign Patents,
 * patent applications, and are protected by trade secret or copyright law.
 * Dissemination of this information, reproduction of this material, and copying or distribution of this software
 * is strictly forbidden unless prior written permission is obtained
 * from Pearson Education, Inc.
 */

var autobahnConsumerSdk = require('@autobahn/gse-autobahn-consumer-sdk');
const { processReceivedData } = require('./autobahnHelper');

/**
 * Create a dataConsumer which gives instance onReady
 * With the instance, initially, stop existing listeners
 * add the event data handler to instance -> the eventHandler (processData should persist the data in mongoDB)
 * then start listening
 * 
 * Learn more: https://docs.google.com/document/d/1WDiByfDD7DBInObK5P1_aCs5eE1JYh3DtODBuPN1ITU/edit#
 * 
 * @author Mohana Sudhan
 */

/**
 * This class instantiates Autobahn queues fetched from the config
 */
class AutobahnConsumer {
    constructor() {
        const queues = ['revel-cl-pmp-loc'];
        console.log({ queues });
        console.log('Starting the Autobahn');
        this.dataConsumerList = queues.map(queue => new autobahnConsumerSdk({
            queueName: queue,
            appName: 'CrystalLake',
            PIAuth: {
                url: 'https://int-piapi.stg-openclass.com/v1/piapi-int',
                userName: 'crystallake_system',
                password: 'PfrMLUddsvMuCW9m8wLBV7cQFXLYkZ4K',
            },
            autobahnAdminServiceUrl: 'https://messaging-admin.stg-prsn.com',
            autobahnMessageStatusServiceUrl: 'https://messaging-status.stg-prsn.com'
        }));
    }

    startListening() {
        const processInstance = (instance) => {
            console.log('Stopped other listeners of the queues');
            instance.stopListening();
            try {
                instance.on('data', processReceivedData);
                instance.startListening();
            } catch (ex) {
                console.error('Error in contacting Autobahn');
            }
            console.log('Started listening Autobahn messages');
        };
        const errorHandler = (error) => {
            console.error('Error in listening Autohabn messages', error);
        };

        //While running in local, the autobahn switch should be false
        //This is because now both local and stg env will be listening the same queue,(Queue -> FIFO)
        //If the local receives first, then the stg env wont receive the data
        this.dataConsumerList.forEach(dataConsumer => dataConsumer.onReady(processInstance, errorHandler));
    }
}
module.exports = AutobahnConsumer;